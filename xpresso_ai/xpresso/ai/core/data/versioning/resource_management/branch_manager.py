from xpresso.ai.core.data.versioning.resource \
    import PachydermResource
from xpresso.ai.core.data.versioning.utils \
    import name_validity_check
from xpresso.ai.core.commons.exceptions.xpr_exceptions \
    import BranchInfoException, CreateBranchException
from xpresso.ai.core.data.versioning.xpr_dv_branch \
    import XprDataVersioningBranch
from xpresso.ai.core.data.versioning.dv_field_name \
    import DataVersioningFieldName as DVFields


class BranchManager(PachydermResource):
    """
    Manages branch and its resources on a pachyderm cluster
    """
    INPUT_BRANCH_NAME = "branch_name"
    OUTPUT_BRANCH_NAME = "branch_name"
    OUTPUT_BRANCH_HEAD = "head"

    def __init__(self, pachyderm_client, persistence_manager):
        super().__init__()
        self.pachyderm_client = pachyderm_client
        self.persistence_manager = persistence_manager
        # string format for datetime object while sending in output
        self.output_datetime_format = "%A, %B %d %Y, %T"

    def create(self, branch_object: XprDataVersioningBranch):
        """
        creates a new branch in specified repo on pachyderm cluster

        Args:
            branch_object:
                Instance of XprDataVersioningBranch class
                with info on the branch
        """
        # repo name and branch name existence checks are already handled
        # before calling this create method
        repo_name = branch_object.get(DVFields.REPO_NAME.value)
        branch_name = branch_object.get(DVFields.BRANCH_NAME.value)
        try:
            self.list(branch_object)
            raise CreateBranchException(
                f"{branch_name} branch already exists in {repo_name} repo"
            )
        except BranchInfoException:
            self.pachyderm_client.create_new_branch(repo_name, branch_name)
            self.add_branch_to_db(branch_object)

    def list(self, branch_object: XprDataVersioningBranch,
             projection_filter: dict = None, limit: int = None):
        """
        returns a list of branches in specified repo

        Args:
            branch_object: Instance of XprDataVersioningBranch
            with the info on branch
            projection_filter: A dictionary to filter the output fields
            limit: limit on number of results
        """
        branch_filter = self.get_branch_filter(branch_object)
        if not projection_filter:
            projection_filter = {
                DVFields.BRANCH_COMMITS_KEY.value: False,
                DVFields.TOTAL_FIlES_IN_BRANCH.value: False,
                DVFields.SIZE_OF_BRANCH.value: False
            }
        branches_list = self.persistence_manager.find(
            "branches", branch_filter, projection_filter)
        if not len(branches_list):
            raise BranchInfoException(
                "No branch found with provided information")
        return self.filter_output(branches_list)

    def delete(self, branch_object: XprDataVersioningBranch):
        """
        deletes a branch from the specified repo

        Args:
            branch_object: Instance of XprDataVersioningBranch with
            branch info
        """
        repo_name = branch_object.get(DVFields.REPO_NAME.value)
        branch_name = branch_object.get(DVFields.BRANCH_NAME.value)
        try:
            self.list(branch_object)
        except BranchInfoException as err:
            err.message = \
                f"{branch_name} branch already not found in {repo_name} repo"
            raise err
        self.pachyderm_client.delete_branch(repo_name, branch_name)
        branch_filter_value = self.get_branch_filter(branch_object)
        self.persistence_manager.delete(
            "branches", branch_filter_value
        )

    def filter_output(self, branch_info):
        """
        takes the list of branches info and returns filtered output

        :param branch_info:
            information on the branch
        :return:
            list of branches filtered
        """
        for branch in branch_info:
            branch[DVFields.BRANCH_CREATED_ON.value] = \
                branch[DVFields.BRANCH_CREATED_ON.value].strftime(
                    self.output_datetime_format)
            branch[self.OUTPUT_BRANCH_HEAD] = \
                str(branch[DVFields.LAST_COMMIT_ID.value]) if \
                branch[DVFields.LAST_COMMIT_ID.value] else '-'
            branch["commit_date"] = branch[DVFields.LAST_COMMIT_ON.value] = \
                branch[DVFields.LAST_COMMIT_ON.value].strftime(
                    self.output_datetime_format) if \
                branch[DVFields.LAST_COMMIT_ID.value] else "-"
        return branch_info

    @staticmethod
    def validate_branch_op_input(branch_op_input):
        """
        validates the input provided for branch level operations

        Args:
            branch_op_input: input provided for the operation
        """
        branch_object = XprDataVersioningBranch(branch_op_input)
        branch_object.validate_mandatory_fields()
        # checks if the branch name is in valid format
        name_validity_check(
            DVFields.BRANCH_NAME.value,
            branch_object.get(DVFields.BRANCH_NAME.value)
        )
        branch_type = branch_object.validate_branch_type()
        if not branch_type:
            # If branch_type is not provided then
            # it is set to BRANCH_TYPE_DATA by default
            branch_type = branch_object.default_branch_type
        branch_object.set(
            DVFields.BRANCH_TYPE.value, branch_type
        )
        return branch_object

    def add_branch_to_db(self, branch_object: XprDataVersioningBranch):
        """
        adds a record in db for a new branch

        Args:
            branch_object: XprDataVersioningBranch Object
            with info on the branch
        """
        # TODO: Add a check to verify persistence manager object
        branch_object.set(
            DVFields.BRANCH_CREATED_BY.value,
            branch_object.get("request_uid")
        )
        branch_record = branch_object.fetch_info_for_db_record()
        self.persistence_manager.insert(
            collection="branches", obj=branch_record.data, duplicate_ok=False
        )

    def add_commit_to_branch(self, branch_object: XprDataVersioningBranch,
                             commit_record: dict):
        """
        add commit info to branches collection

        Args:
            branch_object: info of the branch to which commit needs to be added
            commit_record: info of the commit that needs to be added
        """
        branch_update_info = {
            DVFields.LAST_COMMIT_ID.value:
                commit_record.get(DVFields.XPRESSO_COMMIT_ID.value),
            DVFields.LAST_COMMIT_BY.value:
                commit_record.get(DVFields.COMMITTED_BY.value),
            DVFields.LAST_COMMIT_ON.value:
                commit_record.get(DVFields.COMMITTED_ON.value),
            DVFields.SIZE_OF_BRANCH.value:
                branch_object.get(DVFields.SIZE_OF_BRANCH.value) +
                commit_record.get(DVFields.SIZE_OF_COMMIT.value),
            DVFields.TOTAL_FIlES_IN_BRANCH.value:
                branch_object.get(DVFields.TOTAL_FIlES_IN_BRANCH.value) +
                commit_record.get(DVFields.TOTAL_FILES_IN_COMMIT.value)
        }
        branch_filter = self.get_branch_filter(branch_object)
        self.persistence_manager.update(
            "branches", branch_filter, branch_update_info
        )
        self.persistence_manager.update(
            "branches", branch_filter,
            {DVFields.BRANCH_COMMITS_KEY.value: commit_record},
            flag="push"
        )

    @staticmethod
    def get_branch_filter(branch_object: XprDataVersioningBranch):
        """
        generates a dictionary to filter out the branch from db collection

        Args:
            branch_object: instance of XprDataVersioningBranch
            with info on branch
        Returns:
            returns a dictionary with the info to filter the branch
        """
        key_filter_fields = [
            DVFields.REPO_NAME.value,
            DVFields.BRANCH_NAME.value,
            DVFields.BRANCH_TYPE.value
        ]
        branch_filter = dict()
        for key_field in key_filter_fields:
            key_field_value = branch_object.get(key_field, None)
            if key_field_value:
                branch_filter[key_field] = key_field_value

        return branch_filter
